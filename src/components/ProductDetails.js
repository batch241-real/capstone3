import { useState, useEffect, useContext } from 'react';

import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';

import { useParams, useNavigate, Link } from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function ProductDetails() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const { productId } = useParams();

	const [disabled, setDisabled] = useState(false);
	// const [count, setCount] = useState(0);
	const [quantity, setQuantity] = useState(0);
	const [isClick, setIsClick] = useState(true);
	const [buttonName, setButtonName] = useState("");
	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const pressBtn = (btn) => {
		setDisabled(true);
		setIsClick(false);
		
		if(btn === 'cart'){
			setButtonName('Add To Cart')
		} else{
			setButtonName('Check Out Order')
		}
	}
	
	function action(e){
		e.preventDefault();

			console.log(quantity);
			console.log(productId);

			if(buttonName === 'Add To Cart'){
				
				fetch(`${process.env.REACT_APP_API_URL}/orders/addtocart`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						productId: productId,
						quantity: quantity
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data !== false){
						Swal.fire({
							title: "Added to cart successfully",
							icon: "success",
							text: "You have successfully added a product to cart."
						})
					} else {
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			} else{
				fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						productId: productId,
						quantity: quantity
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data !== false){
						Swal.fire({
							title: "Order check out successfully",
							icon: "success",
							text: "You have successfully check out a order."
						})
					} else {
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}

			setQuantity(0);
			setDisabled(false);
			setIsClick(true);
			navigate('/');
		}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId])

	return (
		<>
		<br/><br/>
		<Container>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card className='card'>
					      <Card.Body className="text-center">
					        <Card.Title className='title'>{productName}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					        {
					        	(user.isAdmin) ?
					        	<>
					        	<Button className="btn btn-secondary" disabled>Add to Cart</Button>&nbsp;&nbsp;&nbsp;
					        	<Button className="btn btn-secondary" disabled>Check Out</Button>
					        	</>
					        	:
					        	(user.id === null) ?
					        	<>
					        	<Button className="btn btn-info" id="loginBtn" as={Link} to={'/login'}>Login to Order</Button>
					        	</>
					        	:
					        	<>
					        	<Button className="btn btn-primary" onClick={() => pressBtn('cart')} hidden={disabled} id='cart'>Add To Cart</Button>&nbsp;&nbsp;&nbsp;
					        	<Button className="btn btn-primary" onClick={() => pressBtn('checkOut')} hidden={disabled} id='checOut'>Check Out</Button>
					        	</>
					        }

				        	<Form className='d-flex justify-content-center' onSubmit={(e) => action(e)}>
				        		<Button variant="secondary"
				        		onClick={() => {
				        			if(quantity<=0){
				        				setQuantity(0)
				        			} else{
				        				setQuantity(quantity - 1)
				        			} }}
				        		hidden={isClick}> - </Button>&nbsp;
      						
      							<Form.Control type="text" placeholder={`${quantity}`} style={{width:'3em'}} className='text-center' hidden={isClick} disabled></Form.Control>&nbsp;
      						
      							<Button variant="secondary" onClick={() => setQuantity(quantity + 1)} hidden={isClick}> + </Button>&nbsp;&nbsp;&nbsp;&nbsp;
      						
      							<Button variant="success" type='submit' hidden={isClick}> {buttonName} </Button>
      						</Form>

					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		</>

	)
}