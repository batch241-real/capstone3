import { useState, useEffect } from 'react';

import {Link} from 'react-router-dom';

import { Card, Container, Row, Col, Button } from 'react-bootstrap';

export default function AllProducts() {

  const [activeProducts, setActiveProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/active`)
    .then(res => res.json())
    .then(data => {
      console.log(data);

      setActiveProducts(data.map(product => {
        return product
      }))
    })
  }, [])

  return (
    <>
    <br/><br/>
    <Container>
    <h1>Products</h1><br/>
    <Row>
    {
      activeProducts.map((data, index) => {
        return(
          <>
            <Col md={3} key={index}>
              <Card className='card'>
                <Card.Body>
                  <Card.Title className='title'>{data.productName}</Card.Title>
                  <Card.Subtitle>Description:</Card.Subtitle>
                  <Card.Text>{data.description}</Card.Text>
                  <Card.Subtitle>Price:</Card.Subtitle>
                  <Card.Text>PhP {data.price}</Card.Text>
                  <Button variant="info" id="btnDetails" as={Link} to={`/products/${data._id}`}> Product Details </Button>
                </Card.Body>
              </Card>
              <br/>
            </Col>
          </>
        )
      })
    }
    </Row>
    </Container>
    </>
  )
}