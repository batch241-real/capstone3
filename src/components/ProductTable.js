import { Table, Button } from 'react-bootstrap';

import BootstrapSwitchButton from 'bootstrap-switch-button-react'

import { useState, useEffect } from 'react'

import { Link } from 'react-router-dom';

export default function ProductTable() {

  const [products, setProducts] = useState([]);

  const archiveProduct = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => console.log(data))
  }
  
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
      headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      setProducts(data.map(product => {
        return product
      }))
    })
  }, [])

  return (
    <>
    <Button variant="primary" as={Link} to={`/addproduct`} active>
        Add Product
      </Button><br/><br/>
    <Table striped bordered hover>
      <thead>
        <tr className='text-center'>
          <th>ID Number</th>
          <th>Product Name</th>
          <th>Description</th>
          <th>Price</th>
          <th>Active</th>
          <th>Created</th>
          <th>Update</th>
        </tr>
      </thead>
      <tbody>
      {
        products.map((data, index) => {
          return(
          <tr key={index}>
            <td>{data._id}</td>
            <td>{data.productName}</td>
            <td>{data.description}</td>
            <td>{data.price}</td>
            <td className='text-center'><BootstrapSwitchButton checked={data.isActive} onChange={() => archiveProduct(data._id)} onstyle="success"/></td>
            <td>{data.createdOn}</td>
            <td className="text-center"><Button as={Link} to={`/update/${data._id}`} variant='danger'>Update</Button></td>
          </tr>     
          )
        })
      }
      </tbody>
    </Table>
    </>
  );
}