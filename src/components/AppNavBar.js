import { useContext } from 'react';

import { Link, NavLink } from 'react-router-dom';

import UserContext from '../UserContext';

import { Nav, Navbar, Container, Image } from 'react-bootstrap';

import { BsFillCartFill, BsFillBagCheckFill} from 'react-icons/bs';


export default function AppNavbar() {

  const { user } = useContext(UserContext);

  return (
    <Navbar bg="light" expand="lg">
    <Container>
        <Navbar.Brand as={Link} to="/">
          <Image src='https://drive.google.com/uc?export=download&id=1Fa29ryoMI1WTMePMZXqxv2rc0KBMgFWQ' style={{height: '3%',  width: '15%'}}/>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
          {
            (user.isAdmin) ?
            <Nav.Link as={NavLink} to="/admin">Dashboard </Nav.Link>
            :
            (user.id !== null) ?
            <>
            <Nav.Link as={NavLink} to="/">Dashboard </Nav.Link>
            <Nav.Link as={NavLink} to="/cart"> <BsFillCartFill/> </Nav.Link>
            <Nav.Link as={NavLink} to="/orders"> <BsFillBagCheckFill/> </Nav.Link>
            </>
            :
            <Nav.Link as={NavLink} to="/">Dashboard </Nav.Link>
          }
            {
              (user.id !== null) ?
              <>
              <Nav.Link as={NavLink} to="/logout"> Logout</Nav.Link>
              </>
              :
              <>
              <Nav.Link as={NavLink} to="/register"> Register </Nav.Link>
              <Nav.Link as={NavLink} to="/login"> Login</Nav.Link>
              </>
            }
          </Nav>
        </Navbar.Collapse>
    </Container>
    </Navbar>
  );
}