import './App.css';

import { useState, useEffect } from 'react';

import { UserProvider } from './UserContext';

import { Container } from 'react-bootstrap';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

// Components
import AppNavBar from './components/AppNavBar'
import ProductDetails from './components/ProductDetails'

// Pages
import Login from './pages/Login'
import Register from './pages/Register'
import Logout from './pages/Logout'
import Admin from './pages/Admin'
import AddProduct from './pages/AddProduct'
import UpdateProduct from './pages/UpdateProduct'
import Dashboard from './pages/Dashboard'
import User from './pages/User'
import AllOrders from './pages/AllOrders'
import Cart from './pages/Cart'
import Orders from './pages/Orders'
import Error from './pages/Error'

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/userdetails`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      // user is logged in
      if(typeof data._id !== 'undefined'){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])
  return (
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavBar/>
          <Container>
            <Routes>
              <Route path='/login' element={<Login/>}/>
            {
              (user.id === null) ?
              <>
              <Route path='/register' element={<Register/>}/>

              </>
              :
              <>
              <Route path='/register' element={<Error/>}/>

              </>
            }
              
              <Route path='/products/:productId' element={<ProductDetails/>}/> 
              <Route path='/logout' element={<Logout/>}/>
              { 
                (user.isAdmin) ?
                <>
                <Route path='/admin' element={<Admin/>}/>
                <Route path='/addproduct' element={<AddProduct/>}/>
                <Route path='/update/:productId' element={<UpdateProduct/>}/>
                <Route path='/allusers' element={<User/>}/>
                <Route path='/allorders' element={<AllOrders/>}/>
                <Route path='/cart' element={<Error/>}/>
                <Route path='/orders' element={<Error/>}/>
                </>
                :
                <>
                <Route path='/admin' element={<Error/>}/>
                <Route path='/addproduct' element={<Error/>}/>
                <Route path='/update/:productId' element={<Error/>}/>
                <Route path='/cart' element={<Cart/>}/>
                <Route path='/orders' element={<Orders/>}/>
                </>
              }
              <Route path='/' element={<Dashboard/>}/>
              <Route path='*' element={<Error/>}/>
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
