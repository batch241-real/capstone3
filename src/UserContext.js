import React from 'react';

// 'React.createContext' - a function in the React Library that creates a new context object.
// 'Context Object' - data type of an object that can be used to store information and can be shared to other components within the app.

const UserContext = React.createContext();


// 'UserContext.Provider' - a component that is created when you use React.createContext().
// 'Provider' - componenet that allows other components to use the context object and/or supply necessary information needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;