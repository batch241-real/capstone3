import { useState, useEffect } from 'react';

import { Card, Container, Row, Col, Button, Form } from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function AllOrders() {

  const [carts, setCarts] = useState([]);
  const [disabled, setDisabled] = useState(true);
  const [hidden, setHidden] = useState(false);
  const [count, setCount] = useState(0);
  const [productId, setProductId] = useState(0)

  const pressBtn = (dataId) => {
    setHidden(true)
    setProductId(dataId)
    setDisabled(false)
  }

  function editQuantity(value){
      setCount(value)
  }

  const changeQuantity = (cartId, quantity) => {
    if(quantity <= 0){
      Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Remove item instead."
          })
    } else{
      fetch(`${process.env.REACT_APP_API_URL}/orders/quantity`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
          cartId: cartId,
          quantity: quantity
        })
      })
      .then(res => res.json())
      .then(data => {
        console.log(data)

        if(data !== false){
          Swal.fire({
            title: "Change quantity successfully",
            icon: "success",
            text: "You have successfully added a product to cart."
          })
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again."
          })
        }
      })
      setDisabled(true)
      setHidden(false)
    }
  }

  const remove = (cartId) => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/removetocart`,{
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        cartId: cartId
      })
    })
    .then(res => res.json)
    .then(data => {
      console.log(data)

      if(data !== false){
          Swal.fire({
            title: "Remove to cart successfully",
            icon: "success",
            text: "You have successfully added a product to cart."
          })
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again."
          })
        }
    })
  } 

  const checkOut = (cartId) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/checkoutcart`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        cartId: cartId
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      if(data !== false){
        Swal.fire({
          title: "Order check out successfully",
          icon: "success",
          text: "You have successfully check out a order."
        })
      } else {
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again."
        })
      }
    })
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/cart`,{
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      setCarts(data.map(cart => {
        return cart
      }))
    })
  }, [carts])

  return (
    <>
    <br/><br/>
    <Container>
    <h1>My Cart</h1><br/>
    <h4>Cart: {carts.length}</h4>
    <Row>
    {
      carts.map((data, index) => {

        return(
          <>
            <Col md={12}key={index}>

              <Card>
                <Card.Body>
                  <Card.Title className='title'>Product Name: {data.productName}</Card.Title>
                  <Card.Text>Quantity:
                   <Form.Control type="number" placeholder={data.quantity} style={{width:'5em'}} onChange={(e) => editQuantity(e.target.value)} disabled={disabled}></Form.Control>
                  </Card.Text>
                  <Card.Text>Total Amount: {data.subTotal}</Card.Text>
                  {
                    (!hidden) ?
                     <>
                    <Button variant="primary" onClick={() => pressBtn(data._id)}> Update </Button>&nbsp;&nbsp;
                    <Button variant="danger" onClick={() => remove(data._id)}> Remove </Button>&nbsp;&nbsp;
                    <Button variant="success" onClick={() => checkOut(data._id)}> Check Out </Button>
                    </>
                    :
                    (data._id === productId) ?
                    <>
                    <Button variant="success" onClick={() => changeQuantity(data._id, count)}> Update </Button>&nbsp;&nbsp;
                    <Button variant="danger" onClick={() => remove(data._id)}> Remove </Button>&nbsp;&nbsp;
                    <Button variant="secondary" onClick={() => checkOut(data._id)} disabled> Check Out </Button>
                    </>
                    :
                    <>
                    <Button variant="primary" onClick={() => pressBtn(data._id)}> Update </Button>&nbsp;&nbsp;
                    <Button variant="danger" onClick={() => remove(data._id)}> Remove </Button>&nbsp;&nbsp;
                    <Button variant="success" onClick={() => checkOut(data._id)}> Check Out </Button>
                    </>
                   
                  }
                </Card.Body>
              </Card>
              <br/>
            </Col>
          </>
        )
      })
    }
    </Row>
    </Container>
    </>
  )
}