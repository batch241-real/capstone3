import { Link } from 'react-router-dom';

import { useContext } from 'react'

import UserContext from '../UserContext'

import { Button, Row, Col } from 'react-bootstrap';

export default function Error() {

    const { user } = useContext(UserContext);

    return (
    <Row>
    	<Col className="p-5">
            <h1>Error 404 - Page not found</h1>
            <p>The page you're trying to access cannot be found.</p>
            {
                (user.isAdmin) ?
                <Link as="Link" to="/admin">
                    <Button variant="primary">Back to Homepage</Button>
                </Link>
                :
                <Link as="Link" to="/">
                    <Button variant="primary">Back to Homepage</Button>
                </Link>

            }
        </Col>
    </Row>
	)
}