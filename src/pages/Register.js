import { useState, useEffect } from 'react';

import { useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2'

import { Button, Form } from 'react-bootstrap';

export default function Register() {

  const navigate = useNavigate();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [verifyPassword, setVerifyPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  function registerUser(e) {
    e.preventDefault()

    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data !== null)

      if(data !== false){
        Swal.fire({
          title: "Registration successful",
          icon: "success",
          text: "Welcome to Zuitt."
        })
      } else {
        Swal.fire({
          title: "Duplicate email found",
          icon: "error",
          text: "Please provide a different email."
        })
      }
      navigate('/login');
    })
        setEmail('');
        setPassword('');
        setVerifyPassword('');

        // alert('Thank you for registering!')
      }

      useEffect(() => {
        // validation to enable submit button when all fields are populated and both passwords match
        if((email !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword)){
          setIsActive(true);
        } else{
          setIsActive(false);
        }
      }, [email, password, verifyPassword])

  return (
    <>
    <br/>
    <center>
    <Form onSubmit={(e) => registerUser(e)} className='form rounded'>
    <h1>Register</h1><br/>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="verifyPassword">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control type="password" placeholder="Verify Password" value={verifyPassword} onChange={e => setVerifyPassword(e.target.value)} required/>
      </Form.Group>
      { isActive ?
        <Button variant="primary" type="submit">
          Signup
        </Button>
        :
        <Button variant="secondary" type="submit">
          Signup
        </Button>
      }
    </Form>
    </center>
    </>
  );
}