import { useState } from 'react';

import { useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2'

import { Button, Form } from 'react-bootstrap';

export default function AddProduct() {

  const navigate = useNavigate();

  const [productName, setProductName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');

  function addProduct(e){
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/addproduct`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        productName: productName,
        description: description,
        price: price
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(data === true){
        Swal.fire({
          title: "Added product successfully",
          icon: "success",
          text: "You have successfully added a product."
        })
      } else {
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again."
        })
      }

      setProductName('');
      setDescription('');
      setPrice('');
      navigate('/admin');
    })
  }

  return (
    <>
    <br/>
    <center>
    <Form onSubmit={(e) => addProduct(e)} className="form rounded">
    <h1>Add Product</h1><br/>
      <Form.Group className="mb-3" controlId="formProductName">
        <Form.Label>Product Name</Form.Label>
        <Form.Control type="text" placeholder="Product name" value={productName} onChange={e => setProductName(e.target.value)} required/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formDescription">
        <Form.Label>Description</Form.Label>
        <Form.Control type="text" placeholder="Description" value={description} onChange={e => setDescription(e.target.value)} required/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formPrice">
        <Form.Label>Price</Form.Label>
        <Form.Control type="text" placeholder="Peso" value={price} onChange={e => setPrice(e.target.value)} required/>
      </Form.Group>

      <Button variant="success" type="submit">
        Add Product
      </Button>
    </Form>
    </center>
    </>
  );
}