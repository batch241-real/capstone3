import { Button, Form } from 'react-bootstrap';

import { useState, useEffect, useContext } from 'react';

import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

function Login() {

  const {user, setUser} = useContext(UserContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  function authenticate(e) {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(typeof data.access !== "undefined"){
                localStorage.setItem('token', data.access)
                retrieveUserDetails(data.access);
        
                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome!"
                })
            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Please, check your login details and try again."
                })

            }
        })

        setEmail('');
        setPassword('');

    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/userdetails`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {
        if(email !== '' && password !== ''){
            setIsActive(true);
        } else{
            setIsActive(false);
        }
    }, [email, password])

  return (
    (user.isAdmin) ?
    <Navigate to='/admin'/>
    :
    (user.id !== null) ?
    <Navigate to='/'/>
    :
    <>
    <br/>
    <center>
    <Form onSubmit={(e) => authenticate(e)} className='form rounded'>
    <h1>Login</h1><br/>
      <Form.Group className="mb-3">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required/>
      </Form.Group>
       { isActive ?
          <Button variant="success" type="submit">
            Login
          </Button>
          :
          <Button variant="secondary" type="submit" disabled>
            Login
          </Button>
        }
    </Form>
    </center>
    </>
  );
}

export default Login;