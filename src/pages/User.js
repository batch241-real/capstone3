import { Table } from 'react-bootstrap';

import BootstrapSwitchButton from 'bootstrap-switch-button-react'

import { useState, useEffect } from 'react'

export default function User() {

	const [users, setUsers] = useState([]);

	const changeToIsAdmin = (userId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/updateuser/${userId}`, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: userId,
			})
		})
		.then(res => res.json())
		.then(data => console.log(data))
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/allusers`,{
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUsers(data.map(user => {
				return user
			}))
		})
	}, [])

	return (
    <>
    <br/>
    <h1>Users</h1><br/>
    <Table striped bordered hover>
      <thead>
        <tr className='text-center'>
          <th>ID Number</th>
          <th>Email</th>
          <th>Admin</th>
        </tr>
      </thead>
      <tbody>
      {
        users.map((data, index) => {
          return(
          <tr key={index} className='text-center'>
            <td>{data._id}</td>
            <td>{data.email}</td>
            <td><BootstrapSwitchButton checked={data.isAdmin} onChange={() => changeToIsAdmin(data._id)} onstyle="success"/></td>
          </tr>     
          )
        })
      }
      </tbody>
    </Table>
    </>
  );
}