import ProductTable from '../components/ProductTable';

import { Card, Col, Row } from 'react-bootstrap';

import { useState, useEffect } from 'react';

import { Link } from 'react-router-dom'

export default function Admin() {

	const [allUsers, setAllUsers] = useState(0);
	const [allOrders, setAllOrders] = useState(0);
	const [allProducts, setAllProducts] = useState(0);


	useEffect(() => {

		// USERS
		fetch(`${process.env.REACT_APP_API_URL}/users/allusers`,{
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setAllUsers(data.length)
		})

		// ORDERS
		fetch(`${process.env.REACT_APP_API_URL}/orders/allorders`,{
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setAllOrders(data.length)
		})

		// PRODUCTS
		fetch(`${process.env.REACT_APP_API_URL}/products/all`,{
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setAllProducts(data.length)
		})
	}, [])

	return(
		<>
		<br/><br/>
	    <h1>Admin Dashboard</h1>
		<Row>
			<Col>
				<Card>
					<Card.Body className="text-center" as={Link} to='/allusers' style={{textDecoration:'none'}}>
						<Card.Text>Users</Card.Text>
						<Card.Title className='titleNumber'>{allUsers}</Card.Title>
					</Card.Body>
				</Card>
			</Col>
			<Col>
				<Card >
					<Card.Body className="text-center" as={Link} to='/allorders' style={{textDecoration:'none'}}>
						<Card.Text>Orders</Card.Text>
						<Card.Title className='titleNumber'>{allOrders}</Card.Title>
					</Card.Body>
				</Card><br/><br/>
			</Col>
			<Col>
				<Card >
					<Card.Body className="text-center">
						<Card.Text>Products</Card.Text>
						<Card.Title className='titleNumber'>{allProducts}</Card.Title>
					</Card.Body>
				</Card><br/><br/>
			</Col>
		</Row>
    	<ProductTable/>
		</>
	)
}