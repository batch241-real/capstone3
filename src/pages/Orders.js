import { useState, useEffect } from 'react';

import { Card, Container, Row, Col } from 'react-bootstrap';

export default function AllOrders() {

  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/usersorder`,{
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      setOrders(data.map(order => {
        return order
      }))
    })
  }, [])

  return (
    <>
    <br/><br/>
    <Container>
    <h1>Order History</h1><br/>
    <h4>Total Orders: {orders.length}</h4>
    <Row>
    {
      orders.map((data, index) => {
        return(
          <>
            <Col md={12}key={index}>
              <Card>
                <Card.Body className='card'>
                  <Card.Title className='title'>Product Name: {data.products[0].productName}</Card.Title><br/>
                  <Card.Text>Quantity: {data.products[0].quantity}</Card.Text>
                  <Card.Text>Purchased Date: {data.purchasedOn}</Card.Text>
                </Card.Body>
              </Card>
              <br/>
            </Col>
          </>
        )
      })
    }
    </Row>
    </Container>
    </>
  )
}